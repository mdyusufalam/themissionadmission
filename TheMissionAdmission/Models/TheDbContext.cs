﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TheMissionAdmission.Models
{
    
    public class TheDbContext:DbContext
    {
        public TheDbContext()
            : base("DefaultConnection")
        {
        }

        public static TheDbContext Create()
        {
            return new TheDbContext();
        }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Contact> Contacts { get; set; }
    }
}