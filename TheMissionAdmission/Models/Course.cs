﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TheMissionAdmission.Models
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        public string  CourseName { get; set; }
        public string Duration { get; set; }
        public string Eligibility { get; set; }
    }
}