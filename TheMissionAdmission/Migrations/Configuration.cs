namespace TheMissionAdmission.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TheMissionAdmission.Models.TheDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TheMissionAdmission.Models.TheDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Courses.AddOrUpdate(
                p => p.Id,

new Course {CourseName = "Fire Officer",  Eligibility = "12th",Duration = "1 Year"},
new Course { CourseName = "Certificate In Fire & Safety",Eligibility="10th", Duration=" 1 Year" },
new Course { CourseName = "Diploma In Fire & Safety", Eligibility = "12th", Duration = " 1 Year"},
new Course
{
    CourseName = "PG Diploma In Fire & Safety", Eligibility = " Graduation",
    Duration = "1 Year"
},
new Course
{
    CourseName = "MBA In Fire & Safety Management", Eligibility = " Graduation",
    Duration = "2 Years"
},
new Course
{
    CourseName = "B.Tech In Fire & Safety Management", Eligibility = " 12th",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Com.", Eligibility = " 12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "M.Com.", Eligibility = "Graduation",
    Duration = "2 Years"
},
new Course
{
    CourseName = "Diploma In Pharmacy", Eligibility = "12th With 45 % (PCB / PCM)",
    Duration = "2 Years"
},
new Course
{
    CourseName = "B.Pharma",
    Eligibility = "12th with 45 % (PCB / PCM)",
    Duration = "4 Years"
},
new Course
{
    CourseName = "M.Pharma",
    Eligibility = "B.Pharma With 50 %",
    Duration = "2 Years"
},
new Course
{
    CourseName = "LLB",
    Eligibility = "Graduation",
    Duration = "3 Years"
},
new Course
{
    CourseName = "LLM",
    Eligibility = "LLB",
    Duration = "2 Years"
},
new Course
{
    CourseName = "Diploma In Mechanical Engineering(PA)",
    Eligibility = "10th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "Diploma In Civil Engineering",
    Eligibility = "10th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "Diploma In Electrical Engineering",
    Eligibility = "10th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "Diploma In Computer Science",
    Eligibility = "10th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "Diploma In Electronics & Communication",
    Eligibility = "10th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "B.Tech.In Mechanical Engineering(PA)",
    Eligibility = "12th",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Tech In Civil Engineering",
    Eligibility = "12th",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Tech.In Electrical Engineering",
    Eligibility = "12th",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.tech.In Computer Science",
    Eligibility = "12th",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Tech.In Electronics & Communication",
    Eligibility = "12th",
    Duration = "4 Years"
},
new Course
{
    CourseName = "M.Tech.In Mechanical Engineering(PA)",
    Eligibility = "B.Tech.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Tech In Civil Engineering",
    Eligibility = "B.Tech.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Tech.In Electrical Engineering",
    Eligibility = "B.Tech.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.tech.In Computer Science",
    Eligibility = "B.Tech.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Tech.In Electronics & Communication",
    Eligibility = "B.Tech.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "BJMC",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "MJMC",
    Eligibility = "BJMC",
    Duration = "2 Years"
},
new Course
{
    CourseName = "B.Sc. (Biology / Microbiology / Physics / Chemistry / Botany / Zoology / Mathematics / Anatomy / Nutrition / Life Science / H.Science / Food Processing / Geography / Fire Safety)",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "B.Sc.In Biotech",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "M.Sc. (Biology / Microbiology / Physics / Chemistry / Botany / Zoology / Mathematics / Anatomy / Nutrition / Life Science / H.Science / Food Processing / Geography / Fire Safety)",
    Eligibility = "B.Sc.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Sc.In Human Nutrition",
    Eligibility = "B.Sc.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "B.Sc. (Biology / Microbiology / Physics / Chemistry / Botany / Zoology / Mathematics / Anatomy / Nutrition / Life Science / H.Science / Food Processing / Geography / Fire Safety)",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "B.Sc.In Biotech",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "M.Sc. (Biology / Microbiology / Physics / Chemistry / Botany / Zoology / Mathematics / Anatomy / Nutrition / Life Science / H.Science / Food Processing / Geography / Fire Safety)",
    Eligibility = "B.Sc.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Sc.In Human Nutrition",
    Eligibility = "B.Sc.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "BPE(Physical Education)",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "MA(Education)",
    Eligibility = "BA / B.Sc.",
    Duration = "2 Years"
},
new Course
{
    CourseName = "MPE(Physical Education)",
    Eligibility = "BA / B.Sc./ BPE",
    Duration = "2 Years"
},
new Course
{
    CourseName = "Diploma In Agriculture",
    Eligibility = "10th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "B.Sc.In Agriculture",
    Eligibility = "12th(PCB / PCM / Agriculture)",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Sc.In Horticulture",
    Eligibility = "12th(PCB / PCM / Agriculture)",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Tech In Agriculture Engineering",
    Eligibility = "12th(PCB / PCM / Agriculture)",
    Duration = "4 Years"
},
new Course
{
    CourseName = "B.Sc.In Forestry",
    Eligibility = "12th(PCB / PCM / Agriculture)",
    Duration = "4 Years"
},
new Course
{
    CourseName = "M.Sc.In Agriculture",
    Eligibility = "B.Sc.In Agriculture",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Sc.In Horticulture",
    Eligibility = "B.Sc.In Agriculture / Horticulture",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Sc.In Plant Breeding & Genetics",
    Eligibility = "B.Sc.In Agriculture",
    Duration = "2 Years"
},
new Course
{
    CourseName = "M.Sc.In Agronomy",
    Eligibility = "B.Sc.In Agriculture",
    Duration = "2 Years"
},
new Course
{
    CourseName = "Diploma In Veterinary & Live Stock Development Assistant(VLDA)",
    Eligibility = "12th",
    Duration = "2 Years"
},
new Course
{
    CourseName = "Diploma In Animal Health Worker",
    Eligibility = "10th",
    Duration = "1 Year"
},
new Course
{
    CourseName = "Certificate In Animal Husbandry Worker(AHW)",
    Eligibility = "10th",
    Duration = "6 Months"
},
new Course
{
    CourseName = "Certificate In Dairy Management",
    Eligibility = "8th",
    Duration = "3 Months"
},
new Course
{
    CourseName = "Diploma In Computer Application(DCA)",
    Eligibility = "12th",
    Duration = "1 Year"
},
new Course
{
    CourseName = "BCA",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "B.Sc. (IT / CS)",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "PG Diploma In Computer Application(PGDCA)",
    Eligibility = "Graduation",
    Duration = "1 Year"
},
new Course
{
    CourseName = "MCA",
    Eligibility = "Graduation",
    Duration = "3 Years"
},
new Course
{
    CourseName = "M.Sc. (IT / CS)",
    Eligibility = "Graduation",
    Duration = "2 Years"
},
new Course
{
    CourseName = "BA(Hindi / English / Sanskrit / Sociology / Political Science / Psychology / History / Geography / Economics / Education)",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "MA(Hindi / English / Sanskrit / Sociology / Political Science / Psychology / History / Geography / Economics / Drawing & Painting / Music / Geography / Education / Journalism & Mass Communication)",
    Eligibility = "Graduation",
    Duration = "2 Years"
},
new Course
{
    CourseName = "MSW",
    Eligibility = "Graduation",
    Duration = "2 Years"
},
new Course
{
    CourseName = "PG Diploma(Business Management / Retail Management)",
    Eligibility = "Graduation",
    Duration = "1 Year"
},
new Course
{
    CourseName = "BBA",
    Eligibility = "12th",
    Duration = "3 Years"
},
new Course
{
    CourseName = "MBA(HR / Finance / Marketing / IT)",
    Eligibility = "Graduation", Duration="2 Years"
}

);



}
    }
}
