﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TheMissionAdmission.Models;

namespace TheMissionAdmission.Controllers
{
    public class HomeController : Controller
    {
        private TheDbContext db = new TheDbContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult OurTeam()
        {
            return View();
        }
        public ActionResult Services()
        {
            return View();
        }
        public ActionResult Testimonials()
        {
            return View();
        }

        public ActionResult Courses()
        {
            return View(db.Courses.ToList().OrderBy(i=>i.CourseName));
        }
        public ActionResult MajorActivities()
        {
            return View();
        }

    }
}